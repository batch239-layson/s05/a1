package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PostService {
    //create a post
    void createPost(String stringToken, Post post);

    //getting all posts
    Iterable<Post> getPosts();

    List<Post> getMyPosts(String stringToken);


    //Edit a user post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    //Delete a user post
    ResponseEntity deletePost(Long id, String stringToken);

    //ResponseEntity retrieveUserPosts(Long id, String stringToken, Post post);

    //List<Post> getMyPost();
}
